#!/usr/bin/env python
# coding: utf-8

# Import required modules
import cv2
from utils import imshow, imshow2
import numpy as np
import matplotlib.pyplot as plt

# Use imread function to read image
img = cv2.imread("../notebooks/data/board.jpg")

# Use imwrite function to write image
cv2.imwrite("../notebooks/data/board_new.jpg",img)

# Create a copy of image
img_copy = img.copy()

# Convert image to grayscale
gray = cv2.cvtColor(img_copy,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
# Detect corners in image
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners
dst = cv2.dilate(dst,None)

img_copy[dst>0.1*dst.max()]=[0,0,255]

imshow(img_copy)

# Save image
cv2.imwrite("data/board_corners.jpg",img_copy)

# Convert image to grayscale
gray = cv2.cvtColor(img_copy,cv2.COLOR_BGR2GRAY)

edges = cv2.Canny(gray,200,600)

imshow2(img,edges)

fig = plt.figure(figsize=(18,10))

# Original image
ax1 = fig.add_subplot(2,2,1)
ax1.imshow(img)

# Convert image to RGB
rgb = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
ax1 = fig.add_subplot(2,2,2)
ax1.imshow(rgb)

# Convert image to grayscale
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ax1 = fig.add_subplot(2,2,3)
ax1.imshow(gray)

# Convert image to HSV color space
hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
ax1 = fig.add_subplot(2,2,4)
ax1.imshow(hsv)

plt.show()

# Getting the dimesnions of the image
dim = img.shape

# Rotating the image by -30 degrees about the center
# dim[0] stores the no of rows and dim[1] no of columns
rotationAngle = -30
scaleFactor = 1

# Rotation matrix
rotationMatrix = cv2.getRotationMatrix2D((dim[1]/2, dim[0]/2), rotationAngle, scaleFactor)

print(rotationMatrix)

result = cv2.warpAffine(img, rotationMatrix, (dim[1],dim[0]))

imshow2(img,result)


# # Thank You!
