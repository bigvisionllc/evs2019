#!/usr/bin/env python
import dlib
import cv2
import numpy as np
from utils import imshow

faceDetector = dlib.get_frontal_face_detector()

landmarkDetector = dlib.shape_predictor("../notebooks/data/shape_predictor_68_face_landmarks.dat")


im = cv2.imread("../notebooks/data/images/woman.jpg")
# Detect faces in the image
faceRects = faceDetector(im, 0)
print("Number of faces detected: ",len(faceRects))

# List to store landmarks of all detected faces
landmarksAll = []

import matplotlib.pyplot as plt

plt.figure(figsize=(15,15))
imDisplay = im.copy()
for r in faceRects:
    cv2.rectangle(imDisplay, (r.left(), r.top()), (r.right(), r.bottom()), (255, 0, 0), 2)

cv2.imshow("Faces",imDisplay)
cv2.waitKey(0)


def drawLandmarks(im, landmarks):
  for i, part in enumerate(landmarks.parts()):
    px = int(part.x)
    py = int(part.y)
    cv2.circle(im, (px, py), 1, (0, 0, 255), thickness=2, lineType=cv2.LINE_AA)
    cv2.putText(im, str(i), (px, py), cv2.FONT_HERSHEY_SIMPLEX, .3, (255, 0, 0), 1)


# Loop over all detected face rectangles
for f in faceRects:
  newRect = dlib.rectangle(int(f.left()),int(f.top()), int(f.right()),int(f.bottom()))

  # For every face rectangle, run landmarkDetector
  landmarks = landmarkDetector(im, newRect)
  
  # Print number of landmarks
  print("Number of landmarks",len(landmarks.parts()))

  # Store landmarks for current face
  landmarksAll.append(landmarks)
  # Draw landmarks on face
  drawLandmarks(im, landmarks)


cv2.imshow("Output",im)
cv2.waitKey(0)
