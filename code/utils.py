import cv2
import matplotlib.pyplot as plt 
from random import randint
import numpy as np

def drawKeypoints(image, keypoints):
    for keypoint in keypoints:
        color = (randint(0,256),randint(0,256),randint(0,256))
        image = drawKeypoint(image, keypoint, color)
    return image

def drawKeypoint (img, keypoint, color):
    draw_shift_bits = 4
    draw_multiplier = 1 << draw_shift_bits

    center = (int(round(keypoint.pt[0])),int(round(keypoint.pt[1])))

    radius = int(round(keypoint.size/2.0))

    # draw the circles around keypoints with the keypoints size
    cv2.circle(img, center, radius, color, 1, cv2.LINE_AA)# draw_shift_bits)

    # draw orientation of the keypoint, if it is applicable
    if keypoint.angle  != -1:

        srcAngleRad = keypoint.angle * np.pi/180.0

        orient = (int(round(np.cos(srcAngleRad)*radius)), \
                 int(round(np.sin(srcAngleRad)*radius)))

        cv2.line(img, center, (center[0]+orient[0],\
                               center[1]+orient[1]),\
                color, 1, cv2.LINE_AA)
    else:
        # draw center with R=1
        radius = 1 * draw_multiplier
        cv2.circle(img, center, radius,\
                  color, 1, cv2.LINE_AA)

    return img

def fixColorSpace(im):
    nDims = len(im.shape)
    if nDims == 3:
        imOut = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    elif nDims == 2: 
        imOut = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
    
    return imOut

def imshow3(im1, im2, im3, scale = 1):
    plt.figure(figsize=(15 * scale, 15 *scale)); 
    
    plt.subplot(131); 
    plt.imshow(fixColorSpace(im1)); 
    plt.axis('off'); 
    
    plt.subplot(132); 
    plt.imshow(fixColorSpace(im2)); 
    plt.axis('off'); 
    
    plt.subplot(133); 
    plt.imshow(fixColorSpace(im3))
    plt.axis('off');

    plt.show()

def imshow2(im1, im2, scale = 1):
    
    imOut1 = fixColorSpace(im1)
    imOut2 = fixColorSpace(im2)

    plt.figure(figsize=(15 * scale, 15 * scale))
    
    plt.subplot(121)
    plt.imshow(imOut1)
    plt.axis('off')
    
    plt.subplot(122)
    plt.imshow(imOut2)
    plt.axis('off')

    plt.show()

def imshow(im, scale = 1):
    imOut = fixColorSpace(im) 
    plt.figure(figsize=(15 * scale, 15 * scale))
    plt.imshow(imOut)
    plt.axis('off')
    plt.show()
